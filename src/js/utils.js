var utils = {}, prefixes = ['Webkit', 'Moz', 'O', 'ms'];

utils.testStyleProperty = function (property) {
    var ucProperty, properties, i;

    ucProperty = property.charAt(0).toUpperCase() + property.slice(1);

    properties = (property + ' ' + prefixes.join(ucProperty + ' ') + ucProperty).split(' ');

    for (i = 0; i < properties.length; i++) {
        if (properties[i] in document.documentElement.style) {
            return properties[i];
        }
    }
};

utils.supportMobile = (function () {
    var regexp, userAgent;

    regexp = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i;
    userAgent = navigator.userAgent.toLowerCase();

    return regexp.test(userAgent);
})();

utils.supportMSPointer = (function () {
    return window.navigator.msPointerEnabled === true;
})();

utils.supportTouch = (function () {
    return 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch
})();

utils.supportClassList = (function () {
    return 'classList' in document.documentElement;
})();

utils.transformPropertyName = (function () {
    return utils.testStyleProperty('transform');
})(utils);

utils.transitionPropertyName = (function () {
    return utils.testStyleProperty('transition');
})(utils);

utils.transitionEndEventName = (function () {
    var map, key;

    map = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'mozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
    };

    for (key in map) {
        if (key in document.documentElement.style) {
            return map[key];
        }
    }
})();

utils.merge = function (object) {
    var args = Array.prototype.slice.call(arguments, 1);

    args.forEach(function (item) {
        for (var key in item) object[key] = item[key];
    });

    return object;
};

utils.toggle = function (value, property) {
    if (property != null) {
        value = !property;
    } else {
        value = value === true;
    }

    return value;
};

utils.debounce = function (callback, wait) {
    var timeout;

    return function () {
        var context, args;

        context = this;
        args = arguments;

        clearTimeout(timeout);

        timeout = setTimeout(function () {
            callback.apply(context, args);
        }, wait);
    }
};

utils.isElement = function (value) {
    return value && typeof value === 'object' && value.nodeType === 1;
};

utils.isFunction = function (value) {
    return value && Object.prototype.toString.call(value) === '[object Function]';
};

utils.addClass = (function () {
    if (utils.supportClassList) {
        return function (el, name) {
            el.classList.add(name);
        }
    } else {
        return function (el, name) {
            var classList;

            name = name.trim();
            classList = el.className.trim().split(/\s+/);

            if (classList.indexOf(name) === -1) {
                classList.push(name);

                el.className = classList.join(' ');
            }
        }
    }
})(utils);

utils.removeClass = (function () {
    if (utils.supportClassList) {
        return function (el, name) {
            el.classList.remove(name);
        }
    } else {
        return function (el, name) {
            var classList, index;

            name = name.trim();
            classList = el.className.trim().split(/\s+/);

            index = classList.indexOf(name);

            if (index !== -1) {
                classList.splice(index, 1);

                el.className = classList.join(' ');
            }
        }
    }
})(utils);

module.exports = utils;
