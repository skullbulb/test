var utils = require('./utils');

function Widget(options) {
    options = this.getOptions(options);

    this.el = options.el || this.createElement();

    this.data = options.data;
    this.options = options;

    if (utils.isFunction(this.initialize)) {
        this.initialize(options);
    }
}

Widget.prototype = {

    getOptions: function (options) {
        var fromAttributes = {};

        options = options || {};

        if ('el' in options) {
            if (!utils.isElement(options.el)) {
                throw new Error('<el> must be a DOM element');
            }

            fromAttributes = this.parseAttributes(options.el);
        }

        return utils.merge({}, this.defaults, fromAttributes, options);
    },

    createElement: function (options) {
        var tagName, el;

        tagName = this.tagName || 'div';

        el = document.createElement(tagName);

        if (this.className) {
            el.className = this.className;
        }

        return el;
    },

    parseAttributes: function (el) {
        var attributes, attribute, key, value, i, data;

        data = {};

        attributes = el.attributes;

        for (i = 0; i < attributes.length; i++) {
            attribute = attributes[i];

            matches = attribute.name.match(/^data-(.+)$/);

            if (matches) {
                key = matches[1];

                if (key in this.defaults) {
                    try {
                        value = JSON.parse(attribute.value.toLowerCase());

                        if (value != null) data[key] = value;
                    } catch (error) {
                        throw new Error('Can not parse [' + attribute.name + '] attribute');
                    }
                }
            }
        }

        return data;
    },

    bindRefs: function () {
        if (utils.isFunction(this.refs)) {
            this.refs = this.refs();
        }
    },

    appendHTML: function (html) {
        this.el.innerHTML = html;
    },

    render: function () {
        var html;

        if (utils.isFunction(this.template)) {
            html = this.template();

            this.appendHTML(html);
        }

        this.bindRefs();
        this.bindEvents();

        if (utils.isFunction(this.afterRender)) {
            this.afterRender();
        }

        return this;
    },

    destroy: function () {
        if (utils.isFunction(this.unbindEvents)) {
            this.unbindEvents();
        }

        if (this.el.parentNode) {
            this.el.parentNode.removeChild(this.el);
        }

        delete this.el;
        delete this.refs;
        delete this.options;
        delete this.data;
    }

};

Widget.extend = function (properties) {
    var Parent, Child;

    Parent = this;

    Child = function () {
        Parent.apply(this, arguments);
    };

    utils.merge(Child.prototype, Parent.prototype, properties);

    return Child;
};

module.exports = Widget;
