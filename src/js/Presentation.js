var Widget = require('./Widget'),
    utils = require('./utils'),

    KB_ESC = 27,
    KB_SPACE = 32,
    KB_LEFT_ARROW = 37,
    KB_RIGHT_ARROW = 39,

    BTN_FIRST = 'first',
    BTN_PREVIOUS = 'previous',
    BTN_PLAYBACK = 'playback',
    BTN_NEXT = 'next',
    BTN_LAST = 'last',
    BTN_FULLSCREEN = 'fullscreen',

    touch = null,
    detected = null,
    scroll = null;

module.exports = Widget.extend({

    index: 0,

    isMaximized: false,

    isToolbarShown: false,

    isPlaying: false,

    className: 'presenation',

    tagName: 'div',

    defaults: {
        // Keyboard navigation
        keyboard: true,
        // Automatic slideshow
        slideshow: true,
        // Slideshow interval
        interval: 3000,
        // Auto hide toolbar in fullscreen mode
        autohide: true,
        // Auto hide timeout
        timeout: 3000,
        // Fullscreen
        fullscreen: true,
        // Width
        width: null,
        // Height
        height: null
    },

    template: function () {
        var html = '';

        if (this.data) {
            html += '<ul class="slides">';

            this.data.forEach(function (item) {
                html += '<li><img src="' + item.src + '" alt="' + item.alt + '"></li>';
            });

            html += '</ul>'
        }

        html += '<div class="toolbar">';

        [BTN_FIRST, BTN_PREVIOUS, BTN_PLAYBACK, BTN_NEXT, BTN_LAST, BTN_FULLSCREEN].forEach(function (name) {
            html += '<button type="button" name="' + name + '" class="btn-' + name + '"></button>';
        });

        html += '</div>'

        return html;
    },

    appendHTML: function (html) {
        var el;

        if (this.data) {
            this.el.innerHTML = html;
        } else {
            el = document.createElement('div');

            el.innerHTML = html;

            this.el.appendChild(el.firstChild);
        }
    },

    refs: function () {
        var container, toolbar;

        container = this.el.getElementsByClassName('slides')[0];
        toolbar = this.el.getElementsByClassName('toolbar')[0];

        return {
            container: container,
            slides: container.getElementsByTagName('li'),
            toolbar: toolbar,
            buttons: toolbar.getElementsByTagName('button')
        };
    },

    bindEvents: function () {
        if (!utils.supportMobile || !utils.supportTouch) {
            this.refs.container.addEventListener('click', this.onContainerClickOrTap.bind(this), true);
            this.refs.toolbar.addEventListener('click', this.onToolbarClickOrTap.bind(this), true);
        }

        if (!utils.supportMobile) {
            if (this.options.keyboard) {
                document.addEventListener('keydown', this.onKeyDown.bind(this), false);
            }

            if (this.options.autohide) {
                this.refs.container.addEventListener('mousemove', this.onContainerMouseMove.bind(this), false);

                this.refs.toolbar.addEventListener('mouseover', this.onToolbarHover.bind(this), false);
                this.refs.toolbar.addEventListener('mouseleave', this.onToolbarHover.bind(this), false);
            }

            if (this.options.slideshow) {
                this.refs.container.addEventListener('mouseover', this.onContainerHover.bind(this), false);
                this.refs.container.addEventListener('mouseleave', this.onContainerHover.bind(this), false);
            }
        }

        if (utils.supportTouch) {
            this.refs.container.addEventListener('touchstart', this.onTouchStart.bind(this), false);
            this.refs.container.addEventListener('touchmove', this.onTouchMove.bind(this), false);
            this.refs.container.addEventListener('touchend', this.onTouchEnd.bind(this), false);

            this.refs.toolbar.addEventListener('touchstart', this.onTouchStart.bind(this), false);
            this.refs.toolbar.addEventListener('touchmove', this.onTouchMove.bind(this), false);
            this.refs.toolbar.addEventListener('touchend', this.onTouchEnd.bind(this), false);

            window.addEventListener('resize', this.onResize.bind(this), false);
        }

        if (utils.transitionEndEventName) {
            this.refs.container.addEventListener(utils.transitionEndEventName, this.onContainerTransitionEnd.bind(this), false);
        }
    },

    unbindEvents: function () {
        if (this.options.keyboard) {
            document.removeEventListener('keydown', this.onKeyDown);
        }

        if (this.options.supportTouch) {
            window.removeEventListener('resize', this.onResize);
        }
    },

    afterRender: function () {
        utils.addClass(this.refs.slides[0], 'show');

        if (this.length() > 1) {
            this.refs.buttons[BTN_FIRST].disabled = true;
            this.refs.buttons[BTN_PREVIOUS].disabled = true;

            if (!this.options.slideshow) {
                this.refs.buttons[BTN_PLAYBACK].disabled = true;
            }

            if (!this.options.fullscreen) {
                this.refs.buttons[BTN_FULLSCREEN].disabled = true;
            }
        } else {
            for (var i; i < this.refs.buttons.length; i++) {
                this.refs.buttons[i].disabled = true;
            }
        }

        if (this.options.height) {
            this.refs.container.style.height = this.options.height + 'px';
        }

        if (this.options.width) {
            this.refs.container.style.width = this.options.width + 'px';
        }

        this.clientWidth = this.refs.container.clientWidth;
    },

    onTouchStart: function (event) {
        event.preventDefault();

        if (event.touches.length === 1 || !touch) {
            touch = {
                pageX: event.changedTouches[0].pageX,
                pageY: event.changedTouches[0].pageY
            }
        }
    },

    onTouchMove: function (event) {
        var deltaX, deltaY, absDeltaX, absDeltaY, threshold;

        threshold = 5;

        if (event.touches.length === 1 && touch) {
            deltaX = event.changedTouches[0].pageX - touch.pageX;
            deltaY = event.changedTouches[0].pageY - touch.pageY;

            absDeltaX = Math.abs(deltaX);
            absDeltaY = Math.abs(deltaY);

            if (detected) {
                if (event.currentTarget === this.refs.container) {
                    this.onContainerDrag(event, deltaX);
                }
            } else if (!scroll) {
                if (absDeltaX > absDeltaY && absDeltaX >= threshold) {
                    detected = true;

                    if (event.currentTarget === this.refs.container) {
                        this.onContainerDragStart(event);
                    }
                } else if (absDeltaY > absDeltaX && absDeltaY >= threshold) {
                    scroll = true;
                }
            }
        }
    },

    onTouchEnd: function (event) {
        var delta = event.changedTouches[0].pageX - touch.pageX;

        if (event.changedTouches.length === 1 && touch) {
            if (event.currentTarget === this.refs.container) {
                if (detected) {
                    this.onContainerDragEnd(event, delta);
                } else if (!scroll) {
                    this.onContainerClickOrTap(event);
                }
            } else if (!detected && !scroll) {
                this.onToolbarClickOrTap(event);
            }

            touch = null;
            detected = null;
            scroll = null;
        }
    },

    onResize: utils.debounce(function (event) {
        this.clientWidth = this.refs.container.clientWidth;
    }, 100),

    onKeyDown: function (event) {
        var key, allowedKeys;

        key = event.keyCode || event.which;

        allowedKeys = [KB_ESC, KB_SPACE, KB_LEFT_ARROW, KB_RIGHT_ARROW];

        if (this.isMaximized && allowedKeys.indexOf(key) !== -1) {
            event.preventDefault();

            if (key === KB_ESC) {
                this.toggleFullscreen(false);
            } else if (key === KB_SPACE) {
                this.togglePlay();
            } else if (key === KB_LEFT_ARROW) {
                this.previous();
            } else if (key === KB_RIGHT_ARROW) {
                this.next();
            }
        }
    },

    onContainerMouseMove: function () {
        if (this.isMaximized && !this.isToolbarShown) {
            this.toggleToolbar(true);

            clearTimeout(this.toolbarTimeout);

            this.toolbarTimeout = setTimeout(this.toggleToolbar.bind(this), this.options.timeout);
        }
    },

    onContainerHover: function (event) {
        if (this.isPlaying && !this.isMaximized) {
            if (event.type === 'mouseover') {
                clearInterval(this.playingInterval);
            } else {
                this.playingInterval = setInterval(this.next.bind(this), this.options.interval);
            }
        }
    },

    onContainerClickOrTap: function (event) {
        event.preventDefault();

        if (this.isMaximized) {
            this.toggleToolbar();
        } else {
            this.togglePlay();
        }
    },

    onContainerDragStart: function (event) {
        var previous;

        utils.addClass(this.refs.container, 'drag');

        // Todo: This code reduces the performance
        previous = this.refs.slides[this.index - 1];

        if (previous) {
            previous.style[utils.transformPropertyName] = 'translate3d(-100%,0,0)';
            previous.style.opacity = 1;
            previous.style.visibility = 'visible';
        }
    },

    onContainerDrag: function (event, delta) {
        var previous, current, next, ratio;

        if (Math.abs(delta) > this.clientWidth) return;

        ratio = Math.abs(delta / this.clientWidth);

        previous = this.refs.slides[this.index - 1];
        current = this.refs.slides[this.index];
        next = this.refs.slides[this.index + 1];

        if (delta < 0) {
            current.style[utils.transformPropertyName] = 'translate3d(' + delta + 'px,0,0)';

            if (next) {
                next.style.opacity = ratio;
            }
        } else {
            current.style.opacity = 1 - ratio;

            if (previous) {
                previous.style[utils.transformPropertyName] = 'translate3d(' + (delta - this.clientWidth) + 'px,0,0)';
            }
        }
    },

    onContainerDragEnd: function (event, delta) {
        var previous, current, next, ratio, threshold, allowed;

        current = this.refs.slides[this.index];
        next = this.refs.slides[this.index + 1];
        previous = this.refs.slides[this.index - 1];

        ratio = Math.abs(delta / this.clientWidth);
        threshold = 0.25;

        utils.removeClass(this.refs.container, 'drag');

        allowed = delta < 0 && next || delta > 0 && previous;

        if (ratio > threshold && allowed) {
            if (delta < 0) {
                current.style[utils.transformPropertyName] = 'translate3d(-100%,0,0)';

                next.removeAttribute('style');

                if (previous) {
                    previous.removeAttribute('style');
                }

                this.next();
            } else {
                previous.style[utils.transformPropertyName] = 'translate3d(0,0,0)';

                current.removeAttribute('style');

                if (next) {
                    next.removeAttribute('style');
                }

                this.previous();
            }
        } else {
            if (delta > 0 && previous) {
                previous.style[utils.transformPropertyName] = 'translate3d(-100%,0,0)';
            }

            if (next) {
                next.removeAttribute('styles');
            }

            current.removeAttribute('style');
        }
    },

    onContainerTransitionEnd: function (event) {
        event.target.removeAttribute('style');
    },

    onToolbarHover: function (event) {
        if (this.isMaximized) {
            if (event.type === 'mouseover') {
                clearTimeout(this.toolbarTimeout);
            } else {
                this.toolbarTimeout = setTimeout(this.toggleToolbar.bind(this), 500);
            }
        }
    },

    onToolbarClickOrTap: function (event) {
        var name = event.target.name;

        if (name === BTN_FIRST) {
            this.first();
        } else if (name === BTN_PREVIOUS) {
            this.previous();
        } else if (name === BTN_PLAYBACK) {
            this.togglePlay();
        } else if (name === BTN_NEXT) {
            this.next();
        } else if (name === BTN_LAST) {
            this.last();
        } else if (name === BTN_FULLSCREEN) {
            this.toggleFullscreen();
        }
    },

    length: function () {
        return this.refs.slides.length;
    },

    showAt: function (index) {
        var length;

        length = this.length();
        index = (index < 0) ? length + index : index;

        if (index !== this.index && index >= 0 && index < length) {
            this.beforeChange(index, this.index, length);

            utils.addClass(this.refs.slides[index], 'show');
            utils.removeClass(this.refs.slides[this.index], 'show');

            this.index = index;
        }
    },

    next: function () {
        this.showAt(this.index + 1);
    },

    previous: function () {
        if (this.index) {
            this.showAt(this.index - 1);
        }
    },

    first: function () {
        this.showAt(0);
    },

    last: function () {
        this.showAt(-1);
    },

    beforeChange: function (newIndex, index, length) {
        var disabled, last;

        last = length - 1;

        if ((disabled = newIndex === 0) || index === 0) {
            this.refs.buttons[BTN_FIRST].disabled = disabled;
            this.refs.buttons[BTN_PREVIOUS].disabled = disabled;
        }

        if ((disabled = newIndex === last) || index === last) {
            this.refs.buttons[BTN_NEXT].disabled = disabled;
            this.refs.buttons[BTN_LAST].disabled = disabled;
        }

        if (this.options.isPlaying && newIndex === last) {
            this.togglePlay(false);
        }

        this.refs.slides[index].style.visibility = 'visible'; // for proper animation
    },

    toggleToolbar: function (show) {
        show = utils.toggle(show, this.isToolbarShown);

        if (this.isToolbarShown !== show) {
            this.isToolbarShown = show;

            if (show) {
                utils.addClass(this.refs.toolbar, 'show');
            } else {
                utils.removeClass(this.refs.toolbar, 'show');
            }
        }
    },

    toggleFullscreen: function (maximize) {
        maximize = utils.toggle(maximize, this.isMaximized);

        if (this.options.fullscreen && this.isMaximized !== maximize) {
            this.isMaximized = maximize;

            if (maximize) {
                utils.addClass(this.el, 'maximize');
                utils.addClass(document.body, 'noscroll');

                // this.toggleToolbar(true);
            } else {
                utils.removeClass(this.el, 'maximize');
                utils.removeClass(document.body, 'noscroll');

                this.toggleToolbar(false);
            }
        }
    },

    togglePlay: function (play) {
        play = utils.toggle(play, this.isPlaying);

        if (this.options.slideshow && this.isPlaying !== play) {
            this.isPlaying = play;

            if (play) {
                utils.addClass(this.el, 'play');

                this.playingInterval = setInterval(this.next.bind(this), this.options.interval);
            } else {
                utils.removeClass(this.el, 'play');

                clearInterval(this.playingInterval);
            }
        }
    }
});
