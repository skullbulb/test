module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        clean: {
            dist: {
                src: './dist/'
            }
        },

        copy: {
            fonts: {
                expand: true,
                cwd: './src/fonts',
                src: '*',
                dest: './dist/fonts/'
            }
        },

        less: {
            styles: {
                src: './src/less/styles.less',
                dest: './dist/css/presentation.css'
            }
        },

        browserify: {
            application: {
                src: './src/js/bootstrap.js',
                dest: './dist/js/presentation.js',
                options: {
                    browserifyOptions: {
                        debug: true
                    }
                }
            }
        },

        watch: {
            options: {
                livereload: true
            },
            demo: {
                files: './demo/*.html'
            },
            styles: {
                files: './src/less/**/*.less',
                tasks: ['less:styles']
            },
            scripts: {
                files: './src/js/**/*.js',
                tasks: ['browserify:application']
            }
        },

        uglify: {
            options: {
                preserveComments: false
            },
            application: {
                options: {
                    compress: true
                },
                src: './dist/js/presentation.js',
                dest: './dist/js/presentation.min.js'
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.registerTask('default', [
        'clean:dist', 'copy:fonts', 'browserify:application', 'less:styles'
    ]);

    grunt.registerTask('release', ['default', 'uglify']);
};
