# Sample app
Lightweight slideshow widget for creating smooth presentations

![showcase](https://dl.dropboxusercontent.com/u/8290751/showcase.jpg)

## Support
- IE 9+
- Mobile devices (Android, iOS)

## Features
- Any size images
- Automatic slideshow
- Keyboard support
- Touchscreen support
- Intuitive controls
- Сustomizable

## Todo
- AMD & Require.js support
- Fullscreen API support
- MSPointer events support
- Transition fallback
- Image preloading
- Themes
- Different Animations

## Usage

```html
<div id ="#some-selector" class="presentation">
	<ul class="slides">
		<li>
			<img src="http://lorempixel.com/g/728/1014" alt="">
		</li>
		<li>
			<img src="http://lorempixel.com/g/1266/1047" alt="">
		</li>
		<li>
			<img src="http://lorempixel.com/g/665/726" alt="">
		</li>
	</ul>
</div>
```

```js
var el = document.getElementById('#some-selector');

new Presentation({ el: el }).render();
```

### Keyboard control
Keyboard control is supported only in full screen mode

`Left arrow`

Show previous slide.

`Right arrow`

Show next slide.

`Space`

Toggle playback

`Esc`

Exit fullscreen

### Touch gestures
`Swipe left`

Show next slide.

`Swipe right`

Show previous slide.

`Tap`

Toggle toolbar in fullscreen mode

### Options
Options can be passed via data attributes or JavaScript. For data attributes, append the option name to `data-`, as in `data-keyboard="true"`.

```js
var defaults = {
	// DOM Element
	el: null,
	// Keyboard navigation (Boolean)
	keyboard: true,
	// Automatic slideshow (Boolean)
	slideshow: true,
	// Slideshow interval (Integer)
	interval: 3000,
	// Auto hide toolbar in fullscreen mode (Boolean)
	autohide: true,
	// Auto hide toolbar timeout (Boolean)
	timeout: 3000,
	// Fullscreen support (Boolean)
	fullscreen: true,
	// Height (Integer) in px
	height: null,
	// Width (Integer) in px
	width: null,
	// Set data (Array)
	data: null
}
```

If `el` is not specified, will be created a new. You need to add it to the DOM manually.

```js
var presentation = new Presentation();

document.body.appendChild(presentations.el);

presentation.render();
```

If `data` is not null, slides will be created from `data`;

```js
var data = [
	{
		src: 'http://lorempixel.com/g/400/300/',
		alt: 'Description'
	},
	{
		src: 'http://lorempixel.com/g/300/200/',
		alt: 'Here'
	}
];

new Presentation({ el: el, data: data }).render();
```

### API
`.showAt(index)`

Show slide with the specific index. The index can be assigned negative values (-1 is the last slide).

`.next()`

Show next slide.

`.previous()`

Show previous slide.

`.first()`

Show first slide.

`.last()`

Show last slide.

`.togglePlay(play)`

Play or pause slideshow.

`.toggleFullscreen(maximize)`

Toggle fullscreen mode

`.toggleToolbar(show)`

Show or hide toolbar.

`.destroy()`

Destroy instance (remove from the DOM and unbind events)


## Links
- <https://developer.mozilla.org>
- <https://developers.google.com>
- <http://stackoverflow.com>
- <http://davidwalsh.name>
- <http://quirksmode.org>
- <http://caniuse.com>
- <http://html5rocks.com>
- <http://lesscss.org>
- <http://gruntjs.com>
- <http://lorempixel.com>
- <http://necolas.github.io/normalize.css>
- <http://www.flaticon.com>
- <http://markup.su>
- <http://getbootstrap.com>

## License
ISC
