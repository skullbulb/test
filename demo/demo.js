// Example 1
new Presentation({ el: document.getElementById('example-1') }).render();

// Example 2
new Presentation({
    el: document.getElementById('example-2'),
    fullscreen: true,
    autohide: false
}).render();

// Example 3
var createButton, destroyButton, container, presentation, options;

createButton = document.getElementById('btn-create');
destroyButton = document.getElementById('btn-destroy');

container = document.getElementById('example-3');

options = {
    slideshow: false,
    fullscreen: false,
    height: 300,
    data: [
        {
            src: 'http://lorempixel.com/g/640/495',
            alt: ''
        },
        {
            src: 'http://lorempixel.com/g/320/495',
            alt: ''
        },
        {
            src: 'http://lorempixel.com/g/550/495',
            alt: ''
        },
        {
            src: 'http://lorempixel.com/g/720/960',
            alt: ''
        },
        {
            src: 'http://lorempixel.com/g/640/1024',
            alt: ''
        }
    ]
};

function onCreateClick(event) {
    event.preventDefault();

    createButton.disabled = true;

    presentation = new Presentation(options);

    container.appendChild(presentation.el);

    presentation.render();

    destroyButton.disabled = false;
}

function onDestroyClick(event) {
    event.preventDefault();

    presentation.destroy();

    createButton.disabled = false;
}

createButton.addEventListener('click', onCreateClick, false);
destroyButton.addEventListener('click', onDestroyClick, false);
