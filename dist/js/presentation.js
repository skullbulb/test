(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var Widget = require('./Widget'),
    utils = require('./utils'),

    KB_ESC = 27,
    KB_SPACE = 32,
    KB_LEFT_ARROW = 37,
    KB_RIGHT_ARROW = 39,

    BTN_FIRST = 'first',
    BTN_PREVIOUS = 'previous',
    BTN_PLAYBACK = 'playback',
    BTN_NEXT = 'next',
    BTN_LAST = 'last',
    BTN_FULLSCREEN = 'fullscreen',

    touch = null,
    detected = null,
    scroll = null;

module.exports = Widget.extend({

    index: 0,

    isMaximized: false,

    isToolbarShown: false,

    isPlaying: false,

    className: 'presenation',

    tagName: 'div',

    defaults: {
        // Keyboard navigation
        keyboard: true,
        // Automatic slideshow
        slideshow: true,
        // Slideshow interval
        interval: 3000,
        // Auto hide toolbar in fullscreen mode
        autohide: true,
        // Auto hide timeout
        timeout: 3000,
        // Fullscreen
        fullscreen: true,
        // Width
        width: null,
        // Height
        height: null
    },

    template: function () {
        var html = '';

        if (this.data) {
            html += '<ul class="slides">';

            this.data.forEach(function (item) {
                html += '<li><img src="' + item.src + '" alt="' + item.alt + '"></li>';
            });

            html += '</ul>'
        }

        html += '<div class="toolbar">';

        [BTN_FIRST, BTN_PREVIOUS, BTN_PLAYBACK, BTN_NEXT, BTN_LAST, BTN_FULLSCREEN].forEach(function (name) {
            html += '<button type="button" name="' + name + '" class="btn-' + name + '"></button>';
        });

        html += '</div>'

        return html;
    },

    appendHTML: function (html) {
        var el;

        if (this.data) {
            this.el.innerHTML = html;
        } else {
            el = document.createElement('div');

            el.innerHTML = html;

            this.el.appendChild(el.firstChild);
        }
    },

    refs: function () {
        var container, toolbar;

        container = this.el.getElementsByClassName('slides')[0];
        toolbar = this.el.getElementsByClassName('toolbar')[0];

        return {
            container: container,
            slides: container.getElementsByTagName('li'),
            toolbar: toolbar,
            buttons: toolbar.getElementsByTagName('button')
        };
    },

    bindEvents: function () {
        if (!utils.supportMobile || !utils.supportTouch) {
            this.refs.container.addEventListener('click', this.onContainerClickOrTap.bind(this), true);
            this.refs.toolbar.addEventListener('click', this.onToolbarClickOrTap.bind(this), true);
        }

        if (!utils.supportMobile) {
            if (this.options.keyboard) {
                document.addEventListener('keydown', this.onKeyDown.bind(this), false);
            }

            if (this.options.autohide) {
                this.refs.container.addEventListener('mousemove', this.onContainerMouseMove.bind(this), false);

                this.refs.toolbar.addEventListener('mouseover', this.onToolbarHover.bind(this), false);
                this.refs.toolbar.addEventListener('mouseleave', this.onToolbarHover.bind(this), false);
            }

            if (this.options.slideshow) {
                this.refs.container.addEventListener('mouseover', this.onContainerHover.bind(this), false);
                this.refs.container.addEventListener('mouseleave', this.onContainerHover.bind(this), false);
            }
        }

        if (utils.supportTouch) {
            this.refs.container.addEventListener('touchstart', this.onTouchStart.bind(this), false);
            this.refs.container.addEventListener('touchmove', this.onTouchMove.bind(this), false);
            this.refs.container.addEventListener('touchend', this.onTouchEnd.bind(this), false);

            this.refs.toolbar.addEventListener('touchstart', this.onTouchStart.bind(this), false);
            this.refs.toolbar.addEventListener('touchmove', this.onTouchMove.bind(this), false);
            this.refs.toolbar.addEventListener('touchend', this.onTouchEnd.bind(this), false);

            window.addEventListener('resize', this.onResize.bind(this), false);
        }

        if (utils.transitionEndEventName) {
            this.refs.container.addEventListener(utils.transitionEndEventName, this.onContainerTransitionEnd.bind(this), false);
        }
    },

    unbindEvents: function () {
        if (this.options.keyboard) {
            document.removeEventListener('keydown', this.onKeyDown);
        }

        if (this.options.supportTouch) {
            window.removeEventListener('resize', this.onResize);
        }
    },

    afterRender: function () {
        utils.addClass(this.refs.slides[0], 'show');

        if (this.length() > 1) {
            this.refs.buttons[BTN_FIRST].disabled = true;
            this.refs.buttons[BTN_PREVIOUS].disabled = true;

            if (!this.options.slideshow) {
                this.refs.buttons[BTN_PLAYBACK].disabled = true;
            }

            if (!this.options.fullscreen) {
                this.refs.buttons[BTN_FULLSCREEN].disabled = true;
            }
        } else {
            for (var i; i < this.refs.buttons.length; i++) {
                this.refs.buttons[i].disabled = true;
            }
        }

        if (this.options.height) {
            this.refs.container.style.height = this.options.height + 'px';
        }

        if (this.options.width) {
            this.refs.container.style.width = this.options.width + 'px';
        }

        this.clientWidth = this.refs.container.clientWidth;
    },

    onTouchStart: function (event) {
        event.preventDefault();

        if (event.touches.length === 1 || !touch) {
            touch = {
                pageX: event.changedTouches[0].pageX,
                pageY: event.changedTouches[0].pageY
            }
        }
    },

    onTouchMove: function (event) {
        var deltaX, deltaY, absDeltaX, absDeltaY, threshold;

        threshold = 5;

        if (event.touches.length === 1 && touch) {
            deltaX = event.changedTouches[0].pageX - touch.pageX;
            deltaY = event.changedTouches[0].pageY - touch.pageY;

            absDeltaX = Math.abs(deltaX);
            absDeltaY = Math.abs(deltaY);

            if (detected) {
                if (event.currentTarget === this.refs.container) {
                    this.onContainerDrag(event, deltaX);
                }
            } else if (!scroll) {
                if (absDeltaX > absDeltaY && absDeltaX >= threshold) {
                    detected = true;

                    if (event.currentTarget === this.refs.container) {
                        this.onContainerDragStart(event);
                    }
                } else if (absDeltaY > absDeltaX && absDeltaY >= threshold) {
                    scroll = true;
                }
            }
        }
    },

    onTouchEnd: function (event) {
        var delta = event.changedTouches[0].pageX - touch.pageX;

        if (event.changedTouches.length === 1 && touch) {
            if (event.currentTarget === this.refs.container) {
                if (detected) {
                    this.onContainerDragEnd(event, delta);
                } else if (!scroll) {
                    this.onContainerClickOrTap(event);
                }
            } else if (!detected && !scroll) {
                this.onToolbarClickOrTap(event);
            }

            touch = null;
            detected = null;
            scroll = null;
        }
    },

    onResize: utils.debounce(function (event) {
        this.clientWidth = this.refs.container.clientWidth;
    }, 100),

    onKeyDown: function (event) {
        var key, allowedKeys;

        key = event.keyCode || event.which;

        allowedKeys = [KB_ESC, KB_SPACE, KB_LEFT_ARROW, KB_RIGHT_ARROW];

        if (this.isMaximized && allowedKeys.indexOf(key) !== -1) {
            event.preventDefault();

            if (key === KB_ESC) {
                this.toggleFullscreen(false);
            } else if (key === KB_SPACE) {
                this.togglePlay();
            } else if (key === KB_LEFT_ARROW) {
                this.previous();
            } else if (key === KB_RIGHT_ARROW) {
                this.next();
            }
        }
    },

    onContainerMouseMove: function () {
        if (this.isMaximized && !this.isToolbarShown) {
            this.toggleToolbar(true);

            clearTimeout(this.toolbarTimeout);

            this.toolbarTimeout = setTimeout(this.toggleToolbar.bind(this), this.options.timeout);
        }
    },

    onContainerHover: function (event) {
        if (this.isPlaying && !this.isMaximized) {
            if (event.type === 'mouseover') {
                clearInterval(this.playingInterval);
            } else {
                this.playingInterval = setInterval(this.next.bind(this), this.options.interval);
            }
        }
    },

    onContainerClickOrTap: function (event) {
        event.preventDefault();

        if (this.isMaximized) {
            this.toggleToolbar();
        } else {
            this.togglePlay();
        }
    },

    onContainerDragStart: function (event) {
        var previous;

        utils.addClass(this.refs.container, 'drag');

        // Todo: This code reduces the performance
        previous = this.refs.slides[this.index - 1];

        if (previous) {
            previous.style[utils.transformPropertyName] = 'translate3d(-100%,0,0)';
            previous.style.opacity = 1;
            previous.style.visibility = 'visible';
        }
    },

    onContainerDrag: function (event, delta) {
        var previous, current, next, ratio;

        if (Math.abs(delta) > this.clientWidth) return;

        ratio = Math.abs(delta / this.clientWidth);

        previous = this.refs.slides[this.index - 1];
        current = this.refs.slides[this.index];
        next = this.refs.slides[this.index + 1];

        if (delta < 0) {
            current.style[utils.transformPropertyName] = 'translate3d(' + delta + 'px,0,0)';

            if (next) {
                next.style.opacity = ratio;
            }
        } else {
            current.style.opacity = 1 - ratio;

            if (previous) {
                previous.style[utils.transformPropertyName] = 'translate3d(' + (delta - this.clientWidth) + 'px,0,0)';
            }
        }
    },

    onContainerDragEnd: function (event, delta) {
        var previous, current, next, ratio, threshold, allowed;

        current = this.refs.slides[this.index];
        next = this.refs.slides[this.index + 1];
        previous = this.refs.slides[this.index - 1];

        ratio = Math.abs(delta / this.clientWidth);
        threshold = 0.25;

        utils.removeClass(this.refs.container, 'drag');

        allowed = delta < 0 && next || delta > 0 && previous;

        if (ratio > threshold && allowed) {
            if (delta < 0) {
                current.style[utils.transformPropertyName] = 'translate3d(-100%,0,0)';

                next.removeAttribute('style');

                if (previous) {
                    previous.removeAttribute('style');
                }

                this.next();
            } else {
                previous.style[utils.transformPropertyName] = 'translate3d(0,0,0)';

                current.removeAttribute('style');

                if (next) {
                    next.removeAttribute('style');
                }

                this.previous();
            }
        } else {
            if (delta > 0 && previous) {
                previous.style[utils.transformPropertyName] = 'translate3d(-100%,0,0)';
            }

            if (next) {
                next.removeAttribute('styles');
            }

            current.removeAttribute('style');
        }
    },

    onContainerTransitionEnd: function (event) {
        event.target.removeAttribute('style');
    },

    onToolbarHover: function (event) {
        if (this.isMaximized) {
            if (event.type === 'mouseover') {
                clearTimeout(this.toolbarTimeout);
            } else {
                this.toolbarTimeout = setTimeout(this.toggleToolbar.bind(this), 500);
            }
        }
    },

    onToolbarClickOrTap: function (event) {
        var name = event.target.name;

        if (name === BTN_FIRST) {
            this.first();
        } else if (name === BTN_PREVIOUS) {
            this.previous();
        } else if (name === BTN_PLAYBACK) {
            this.togglePlay();
        } else if (name === BTN_NEXT) {
            this.next();
        } else if (name === BTN_LAST) {
            this.last();
        } else if (name === BTN_FULLSCREEN) {
            this.toggleFullscreen();
        }
    },

    length: function () {
        return this.refs.slides.length;
    },

    showAt: function (index) {
        var length;

        length = this.length();
        index = (index < 0) ? length + index : index;

        if (index !== this.index && index >= 0 && index < length) {
            this.beforeChange(index, this.index, length);

            utils.addClass(this.refs.slides[index], 'show');
            utils.removeClass(this.refs.slides[this.index], 'show');

            this.index = index;
        }
    },

    next: function () {
        this.showAt(this.index + 1);
    },

    previous: function () {
        if (this.index) {
            this.showAt(this.index - 1);
        }
    },

    first: function () {
        this.showAt(0);
    },

    last: function () {
        this.showAt(-1);
    },

    beforeChange: function (newIndex, index, length) {
        var disabled, last;

        last = length - 1;

        if ((disabled = newIndex === 0) || index === 0) {
            this.refs.buttons[BTN_FIRST].disabled = disabled;
            this.refs.buttons[BTN_PREVIOUS].disabled = disabled;
        }

        if ((disabled = newIndex === last) || index === last) {
            this.refs.buttons[BTN_NEXT].disabled = disabled;
            this.refs.buttons[BTN_LAST].disabled = disabled;
        }

        if (this.options.isPlaying && newIndex === last) {
            this.togglePlay(false);
        }

        this.refs.slides[index].style.visibility = 'visible'; // for proper animation
    },

    toggleToolbar: function (show) {
        show = utils.toggle(show, this.isToolbarShown);

        if (this.isToolbarShown !== show) {
            this.isToolbarShown = show;

            if (show) {
                utils.addClass(this.refs.toolbar, 'show');
            } else {
                utils.removeClass(this.refs.toolbar, 'show');
            }
        }
    },

    toggleFullscreen: function (maximize) {
        maximize = utils.toggle(maximize, this.isMaximized);

        if (this.options.fullscreen && this.isMaximized !== maximize) {
            this.isMaximized = maximize;

            if (maximize) {
                utils.addClass(this.el, 'maximize');
                utils.addClass(document.body, 'noscroll');

                // this.toggleToolbar(true);
            } else {
                utils.removeClass(this.el, 'maximize');
                utils.removeClass(document.body, 'noscroll');

                this.toggleToolbar(false);
            }
        }
    },

    togglePlay: function (play) {
        play = utils.toggle(play, this.isPlaying);

        if (this.options.slideshow && this.isPlaying !== play) {
            this.isPlaying = play;

            if (play) {
                utils.addClass(this.el, 'play');

                this.playingInterval = setInterval(this.next.bind(this), this.options.interval);
            } else {
                utils.removeClass(this.el, 'play');

                clearInterval(this.playingInterval);
            }
        }
    }
});

},{"./Widget":2,"./utils":4}],2:[function(require,module,exports){
var utils = require('./utils');

function Widget(options) {
    options = this.getOptions(options);

    this.el = options.el || this.createElement();

    this.data = options.data;
    this.options = options;

    if (utils.isFunction(this.initialize)) {
        this.initialize(options);
    }
}

Widget.prototype = {

    getOptions: function (options) {
        var fromAttributes = {};

        options = options || {};

        if ('el' in options) {
            if (!utils.isElement(options.el)) {
                throw new Error('<el> must be a DOM element');
            }

            fromAttributes = this.parseAttributes(options.el);
        }

        return utils.merge({}, this.defaults, fromAttributes, options);
    },

    createElement: function (options) {
        var tagName, el;

        tagName = this.tagName || 'div';

        el = document.createElement(tagName);

        if (this.className) {
            el.className = this.className;
        }

        return el;
    },

    parseAttributes: function (el) {
        var attributes, attribute, key, value, i, data;

        data = {};

        attributes = el.attributes;

        for (i = 0; i < attributes.length; i++) {
            attribute = attributes[i];

            matches = attribute.name.match(/^data-(.+)$/);

            if (matches) {
                key = matches[1];

                if (key in this.defaults) {
                    try {
                        value = JSON.parse(attribute.value.toLowerCase());

                        if (value != null) data[key] = value;
                    } catch (error) {
                        throw new Error('Can not parse [' + attribute.name + '] attribute');
                    }
                }
            }
        }

        return data;
    },

    bindRefs: function () {
        if (utils.isFunction(this.refs)) {
            this.refs = this.refs();
        }
    },

    appendHTML: function (html) {
        this.el.innerHTML = html;
    },

    render: function () {
        var html;

        if (utils.isFunction(this.template)) {
            html = this.template();

            this.appendHTML(html);
        }

        this.bindRefs();
        this.bindEvents();

        if (utils.isFunction(this.afterRender)) {
            this.afterRender();
        }

        return this;
    },

    destroy: function () {
        if (utils.isFunction(this.unbindEvents)) {
            this.unbindEvents();
        }

        if (this.el.parentNode) {
            this.el.parentNode.removeChild(this.el);
        }

        delete this.el;
        delete this.refs;
        delete this.options;
        delete this.data;
    }

};

Widget.extend = function (properties) {
    var Parent, Child;

    Parent = this;

    Child = function () {
        Parent.apply(this, arguments);
    };

    utils.merge(Child.prototype, Parent.prototype, properties);

    return Child;
};

module.exports = Widget;

},{"./utils":4}],3:[function(require,module,exports){
window.Presentation = require('./Presentation');

},{"./Presentation":1}],4:[function(require,module,exports){
var utils = {}, prefixes = ['Webkit', 'Moz', 'O', 'ms'];

utils.testStyleProperty = function (property) {
    var ucProperty, properties, i;

    ucProperty = property.charAt(0).toUpperCase() + property.slice(1);

    properties = (property + ' ' + prefixes.join(ucProperty + ' ') + ucProperty).split(' ');

    for (i = 0; i < properties.length; i++) {
        if (properties[i] in document.documentElement.style) {
            return properties[i];
        }
    }
};

utils.supportMobile = (function () {
    var regexp, userAgent;

    regexp = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i;
    userAgent = navigator.userAgent.toLowerCase();

    return regexp.test(userAgent);
})();

utils.supportMSPointer = (function () {
    return window.navigator.msPointerEnabled === true;
})();

utils.supportTouch = (function () {
    return 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch
})();

utils.supportClassList = (function () {
    return 'classList' in document.documentElement;
})();

utils.transformPropertyName = (function () {
    return utils.testStyleProperty('transform');
})(utils);

utils.transitionPropertyName = (function () {
    return utils.testStyleProperty('transition');
})(utils);

utils.transitionEndEventName = (function () {
    var map, key;

    map = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'mozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
    };

    for (key in map) {
        if (key in document.documentElement.style) {
            return map[key];
        }
    }
})();

utils.merge = function (object) {
    var args = Array.prototype.slice.call(arguments, 1);

    args.forEach(function (item) {
        for (var key in item) object[key] = item[key];
    });

    return object;
};

utils.toggle = function (value, property) {
    if (property != null) {
        value = !property;
    } else {
        value = value === true;
    }

    return value;
};

utils.debounce = function (callback, wait) {
    var timeout;

    return function () {
        var context, args;

        context = this;
        args = arguments;

        clearTimeout(timeout);

        timeout = setTimeout(function () {
            callback.apply(context, args);
        }, wait);
    }
};

utils.isElement = function (value) {
    return value && typeof value === 'object' && value.nodeType === 1;
};

utils.isFunction = function (value) {
    return value && Object.prototype.toString.call(value) === '[object Function]';
};

utils.addClass = (function () {
    if (utils.supportClassList) {
        return function (el, name) {
            el.classList.add(name);
        }
    } else {
        return function (el, name) {
            var classList;

            name = name.trim();
            classList = el.className.trim().split(/\s+/);

            if (classList.indexOf(name) === -1) {
                classList.push(name);

                el.className = classList.join(' ');
            }
        }
    }
})(utils);

utils.removeClass = (function () {
    if (utils.supportClassList) {
        return function (el, name) {
            el.classList.remove(name);
        }
    } else {
        return function (el, name) {
            var classList, index;

            name = name.trim();
            classList = el.className.trim().split(/\s+/);

            index = classList.indexOf(name);

            if (index !== -1) {
                classList.splice(index, 1);

                el.className = classList.join(' ');
            }
        }
    }
})(utils);

module.exports = utils;

},{}]},{},[3])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9ncnVudC1icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvUHJlc2VudGF0aW9uLmpzIiwic3JjL2pzL1dpZGdldC5qcyIsInNyYy9qcy9ib290c3RyYXAuanMiLCJzcmMvanMvdXRpbHMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6aEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFJQTtBQUNBOztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgV2lkZ2V0ID0gcmVxdWlyZSgnLi9XaWRnZXQnKSxcbiAgICB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKSxcblxuICAgIEtCX0VTQyA9IDI3LFxuICAgIEtCX1NQQUNFID0gMzIsXG4gICAgS0JfTEVGVF9BUlJPVyA9IDM3LFxuICAgIEtCX1JJR0hUX0FSUk9XID0gMzksXG5cbiAgICBCVE5fRklSU1QgPSAnZmlyc3QnLFxuICAgIEJUTl9QUkVWSU9VUyA9ICdwcmV2aW91cycsXG4gICAgQlROX1BMQVlCQUNLID0gJ3BsYXliYWNrJyxcbiAgICBCVE5fTkVYVCA9ICduZXh0JyxcbiAgICBCVE5fTEFTVCA9ICdsYXN0JyxcbiAgICBCVE5fRlVMTFNDUkVFTiA9ICdmdWxsc2NyZWVuJyxcblxuICAgIHRvdWNoID0gbnVsbCxcbiAgICBkZXRlY3RlZCA9IG51bGwsXG4gICAgc2Nyb2xsID0gbnVsbDtcblxubW9kdWxlLmV4cG9ydHMgPSBXaWRnZXQuZXh0ZW5kKHtcblxuICAgIGluZGV4OiAwLFxuXG4gICAgaXNNYXhpbWl6ZWQ6IGZhbHNlLFxuXG4gICAgaXNUb29sYmFyU2hvd246IGZhbHNlLFxuXG4gICAgaXNQbGF5aW5nOiBmYWxzZSxcblxuICAgIGNsYXNzTmFtZTogJ3ByZXNlbmF0aW9uJyxcblxuICAgIHRhZ05hbWU6ICdkaXYnLFxuXG4gICAgZGVmYXVsdHM6IHtcbiAgICAgICAgLy8gS2V5Ym9hcmQgbmF2aWdhdGlvblxuICAgICAgICBrZXlib2FyZDogdHJ1ZSxcbiAgICAgICAgLy8gQXV0b21hdGljIHNsaWRlc2hvd1xuICAgICAgICBzbGlkZXNob3c6IHRydWUsXG4gICAgICAgIC8vIFNsaWRlc2hvdyBpbnRlcnZhbFxuICAgICAgICBpbnRlcnZhbDogMzAwMCxcbiAgICAgICAgLy8gQXV0byBoaWRlIHRvb2xiYXIgaW4gZnVsbHNjcmVlbiBtb2RlXG4gICAgICAgIGF1dG9oaWRlOiB0cnVlLFxuICAgICAgICAvLyBBdXRvIGhpZGUgdGltZW91dFxuICAgICAgICB0aW1lb3V0OiAzMDAwLFxuICAgICAgICAvLyBGdWxsc2NyZWVuXG4gICAgICAgIGZ1bGxzY3JlZW46IHRydWUsXG4gICAgICAgIC8vIFdpZHRoXG4gICAgICAgIHdpZHRoOiBudWxsLFxuICAgICAgICAvLyBIZWlnaHRcbiAgICAgICAgaGVpZ2h0OiBudWxsXG4gICAgfSxcblxuICAgIHRlbXBsYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBodG1sID0gJyc7XG5cbiAgICAgICAgaWYgKHRoaXMuZGF0YSkge1xuICAgICAgICAgICAgaHRtbCArPSAnPHVsIGNsYXNzPVwic2xpZGVzXCI+JztcblxuICAgICAgICAgICAgdGhpcy5kYXRhLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICBodG1sICs9ICc8bGk+PGltZyBzcmM9XCInICsgaXRlbS5zcmMgKyAnXCIgYWx0PVwiJyArIGl0ZW0uYWx0ICsgJ1wiPjwvbGk+JztcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBodG1sICs9ICc8L3VsPidcbiAgICAgICAgfVxuXG4gICAgICAgIGh0bWwgKz0gJzxkaXYgY2xhc3M9XCJ0b29sYmFyXCI+JztcblxuICAgICAgICBbQlROX0ZJUlNULCBCVE5fUFJFVklPVVMsIEJUTl9QTEFZQkFDSywgQlROX05FWFQsIEJUTl9MQVNULCBCVE5fRlVMTFNDUkVFTl0uZm9yRWFjaChmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICAgICAgaHRtbCArPSAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgbmFtZT1cIicgKyBuYW1lICsgJ1wiIGNsYXNzPVwiYnRuLScgKyBuYW1lICsgJ1wiPjwvYnV0dG9uPic7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGh0bWwgKz0gJzwvZGl2PidcblxuICAgICAgICByZXR1cm4gaHRtbDtcbiAgICB9LFxuXG4gICAgYXBwZW5kSFRNTDogZnVuY3Rpb24gKGh0bWwpIHtcbiAgICAgICAgdmFyIGVsO1xuXG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgICAgICAgIHRoaXMuZWwuaW5uZXJIVE1MID0gaHRtbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cbiAgICAgICAgICAgIGVsLmlubmVySFRNTCA9IGh0bWw7XG5cbiAgICAgICAgICAgIHRoaXMuZWwuYXBwZW5kQ2hpbGQoZWwuZmlyc3RDaGlsZCk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcmVmczogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgY29udGFpbmVyLCB0b29sYmFyO1xuXG4gICAgICAgIGNvbnRhaW5lciA9IHRoaXMuZWwuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnc2xpZGVzJylbMF07XG4gICAgICAgIHRvb2xiYXIgPSB0aGlzLmVsLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3Rvb2xiYXInKVswXTtcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY29udGFpbmVyOiBjb250YWluZXIsXG4gICAgICAgICAgICBzbGlkZXM6IGNvbnRhaW5lci5nZXRFbGVtZW50c0J5VGFnTmFtZSgnbGknKSxcbiAgICAgICAgICAgIHRvb2xiYXI6IHRvb2xiYXIsXG4gICAgICAgICAgICBidXR0b25zOiB0b29sYmFyLmdldEVsZW1lbnRzQnlUYWdOYW1lKCdidXR0b24nKVxuICAgICAgICB9O1xuICAgIH0sXG5cbiAgICBiaW5kRXZlbnRzOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICghdXRpbHMuc3VwcG9ydE1vYmlsZSB8fCAhdXRpbHMuc3VwcG9ydFRvdWNoKSB7XG4gICAgICAgICAgICB0aGlzLnJlZnMuY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5vbkNvbnRhaW5lckNsaWNrT3JUYXAuYmluZCh0aGlzKSwgdHJ1ZSk7XG4gICAgICAgICAgICB0aGlzLnJlZnMudG9vbGJhci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMub25Ub29sYmFyQ2xpY2tPclRhcC5iaW5kKHRoaXMpLCB0cnVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghdXRpbHMuc3VwcG9ydE1vYmlsZSkge1xuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5rZXlib2FyZCkge1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLm9uS2V5RG93bi5iaW5kKHRoaXMpLCBmYWxzZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYXV0b2hpZGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlZnMuY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIHRoaXMub25Db250YWluZXJNb3VzZU1vdmUuYmluZCh0aGlzKSwgZmFsc2UpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZWZzLnRvb2xiYXIuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VvdmVyJywgdGhpcy5vblRvb2xiYXJIb3Zlci5iaW5kKHRoaXMpLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZWZzLnRvb2xiYXIuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIHRoaXMub25Ub29sYmFySG92ZXIuYmluZCh0aGlzKSwgZmFsc2UpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnNsaWRlc2hvdykge1xuICAgICAgICAgICAgICAgIHRoaXMucmVmcy5jb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VvdmVyJywgdGhpcy5vbkNvbnRhaW5lckhvdmVyLmJpbmQodGhpcyksIGZhbHNlKTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlZnMuY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCB0aGlzLm9uQ29udGFpbmVySG92ZXIuYmluZCh0aGlzKSwgZmFsc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHV0aWxzLnN1cHBvcnRUb3VjaCkge1xuICAgICAgICAgICAgdGhpcy5yZWZzLmNvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0JywgdGhpcy5vblRvdWNoU3RhcnQuYmluZCh0aGlzKSwgZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5yZWZzLmNvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCB0aGlzLm9uVG91Y2hNb3ZlLmJpbmQodGhpcyksIGZhbHNlKTtcbiAgICAgICAgICAgIHRoaXMucmVmcy5jb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCB0aGlzLm9uVG91Y2hFbmQuYmluZCh0aGlzKSwgZmFsc2UpO1xuXG4gICAgICAgICAgICB0aGlzLnJlZnMudG9vbGJhci5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0JywgdGhpcy5vblRvdWNoU3RhcnQuYmluZCh0aGlzKSwgZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5yZWZzLnRvb2xiYXIuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2htb3ZlJywgdGhpcy5vblRvdWNoTW92ZS5iaW5kKHRoaXMpLCBmYWxzZSk7XG4gICAgICAgICAgICB0aGlzLnJlZnMudG9vbGJhci5hZGRFdmVudExpc3RlbmVyKCd0b3VjaGVuZCcsIHRoaXMub25Ub3VjaEVuZC5iaW5kKHRoaXMpLCBmYWxzZSk7XG5cbiAgICAgICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLm9uUmVzaXplLmJpbmQodGhpcyksIGZhbHNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh1dGlscy50cmFuc2l0aW9uRW5kRXZlbnROYW1lKSB7XG4gICAgICAgICAgICB0aGlzLnJlZnMuY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIodXRpbHMudHJhbnNpdGlvbkVuZEV2ZW50TmFtZSwgdGhpcy5vbkNvbnRhaW5lclRyYW5zaXRpb25FbmQuYmluZCh0aGlzKSwgZmFsc2UpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIHVuYmluZEV2ZW50czogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmtleWJvYXJkKSB7XG4gICAgICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdrZXlkb3duJywgdGhpcy5vbktleURvd24pO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5zdXBwb3J0VG91Y2gpIHtcbiAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLm9uUmVzaXplKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBhZnRlclJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICB1dGlscy5hZGRDbGFzcyh0aGlzLnJlZnMuc2xpZGVzWzBdLCAnc2hvdycpO1xuXG4gICAgICAgIGlmICh0aGlzLmxlbmd0aCgpID4gMSkge1xuICAgICAgICAgICAgdGhpcy5yZWZzLmJ1dHRvbnNbQlROX0ZJUlNUXS5kaXNhYmxlZCA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLnJlZnMuYnV0dG9uc1tCVE5fUFJFVklPVVNdLmRpc2FibGVkID0gdHJ1ZTtcblxuICAgICAgICAgICAgaWYgKCF0aGlzLm9wdGlvbnMuc2xpZGVzaG93KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZWZzLmJ1dHRvbnNbQlROX1BMQVlCQUNLXS5kaXNhYmxlZCA9IHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICghdGhpcy5vcHRpb25zLmZ1bGxzY3JlZW4pIHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlZnMuYnV0dG9uc1tCVE5fRlVMTFNDUkVFTl0uZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZm9yICh2YXIgaTsgaSA8IHRoaXMucmVmcy5idXR0b25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZWZzLmJ1dHRvbnNbaV0uZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5oZWlnaHQpIHtcbiAgICAgICAgICAgIHRoaXMucmVmcy5jb250YWluZXIuc3R5bGUuaGVpZ2h0ID0gdGhpcy5vcHRpb25zLmhlaWdodCArICdweCc7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLndpZHRoKSB7XG4gICAgICAgICAgICB0aGlzLnJlZnMuY29udGFpbmVyLnN0eWxlLndpZHRoID0gdGhpcy5vcHRpb25zLndpZHRoICsgJ3B4JztcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuY2xpZW50V2lkdGggPSB0aGlzLnJlZnMuY29udGFpbmVyLmNsaWVudFdpZHRoO1xuICAgIH0sXG5cbiAgICBvblRvdWNoU3RhcnQ6IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIGlmIChldmVudC50b3VjaGVzLmxlbmd0aCA9PT0gMSB8fCAhdG91Y2gpIHtcbiAgICAgICAgICAgIHRvdWNoID0ge1xuICAgICAgICAgICAgICAgIHBhZ2VYOiBldmVudC5jaGFuZ2VkVG91Y2hlc1swXS5wYWdlWCxcbiAgICAgICAgICAgICAgICBwYWdlWTogZXZlbnQuY2hhbmdlZFRvdWNoZXNbMF0ucGFnZVlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBvblRvdWNoTW92ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciBkZWx0YVgsIGRlbHRhWSwgYWJzRGVsdGFYLCBhYnNEZWx0YVksIHRocmVzaG9sZDtcblxuICAgICAgICB0aHJlc2hvbGQgPSA1O1xuXG4gICAgICAgIGlmIChldmVudC50b3VjaGVzLmxlbmd0aCA9PT0gMSAmJiB0b3VjaCkge1xuICAgICAgICAgICAgZGVsdGFYID0gZXZlbnQuY2hhbmdlZFRvdWNoZXNbMF0ucGFnZVggLSB0b3VjaC5wYWdlWDtcbiAgICAgICAgICAgIGRlbHRhWSA9IGV2ZW50LmNoYW5nZWRUb3VjaGVzWzBdLnBhZ2VZIC0gdG91Y2gucGFnZVk7XG5cbiAgICAgICAgICAgIGFic0RlbHRhWCA9IE1hdGguYWJzKGRlbHRhWCk7XG4gICAgICAgICAgICBhYnNEZWx0YVkgPSBNYXRoLmFicyhkZWx0YVkpO1xuXG4gICAgICAgICAgICBpZiAoZGV0ZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQuY3VycmVudFRhcmdldCA9PT0gdGhpcy5yZWZzLmNvbnRhaW5lcikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uQ29udGFpbmVyRHJhZyhldmVudCwgZGVsdGFYKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKCFzY3JvbGwpIHtcbiAgICAgICAgICAgICAgICBpZiAoYWJzRGVsdGFYID4gYWJzRGVsdGFZICYmIGFic0RlbHRhWCA+PSB0aHJlc2hvbGQpIHtcbiAgICAgICAgICAgICAgICAgICAgZGV0ZWN0ZWQgPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudC5jdXJyZW50VGFyZ2V0ID09PSB0aGlzLnJlZnMuY29udGFpbmVyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9uQ29udGFpbmVyRHJhZ1N0YXJ0KGV2ZW50KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYWJzRGVsdGFZID4gYWJzRGVsdGFYICYmIGFic0RlbHRhWSA+PSB0aHJlc2hvbGQpIHtcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgb25Ub3VjaEVuZDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciBkZWx0YSA9IGV2ZW50LmNoYW5nZWRUb3VjaGVzWzBdLnBhZ2VYIC0gdG91Y2gucGFnZVg7XG5cbiAgICAgICAgaWYgKGV2ZW50LmNoYW5nZWRUb3VjaGVzLmxlbmd0aCA9PT0gMSAmJiB0b3VjaCkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LmN1cnJlbnRUYXJnZXQgPT09IHRoaXMucmVmcy5jb250YWluZXIpIHtcbiAgICAgICAgICAgICAgICBpZiAoZGV0ZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkNvbnRhaW5lckRyYWdFbmQoZXZlbnQsIGRlbHRhKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCFzY3JvbGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkNvbnRhaW5lckNsaWNrT3JUYXAoZXZlbnQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIWRldGVjdGVkICYmICFzY3JvbGwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uVG9vbGJhckNsaWNrT3JUYXAoZXZlbnQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0b3VjaCA9IG51bGw7XG4gICAgICAgICAgICBkZXRlY3RlZCA9IG51bGw7XG4gICAgICAgICAgICBzY3JvbGwgPSBudWxsO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIG9uUmVzaXplOiB1dGlscy5kZWJvdW5jZShmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgdGhpcy5jbGllbnRXaWR0aCA9IHRoaXMucmVmcy5jb250YWluZXIuY2xpZW50V2lkdGg7XG4gICAgfSwgMTAwKSxcblxuICAgIG9uS2V5RG93bjogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciBrZXksIGFsbG93ZWRLZXlzO1xuXG4gICAgICAgIGtleSA9IGV2ZW50LmtleUNvZGUgfHwgZXZlbnQud2hpY2g7XG5cbiAgICAgICAgYWxsb3dlZEtleXMgPSBbS0JfRVNDLCBLQl9TUEFDRSwgS0JfTEVGVF9BUlJPVywgS0JfUklHSFRfQVJST1ddO1xuXG4gICAgICAgIGlmICh0aGlzLmlzTWF4aW1pemVkICYmIGFsbG93ZWRLZXlzLmluZGV4T2Yoa2V5KSAhPT0gLTEpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgIGlmIChrZXkgPT09IEtCX0VTQykge1xuICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlRnVsbHNjcmVlbihmYWxzZSk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGtleSA9PT0gS0JfU1BBQ0UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZVBsYXkoKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoa2V5ID09PSBLQl9MRUZUX0FSUk9XKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wcmV2aW91cygpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChrZXkgPT09IEtCX1JJR0hUX0FSUk9XKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5uZXh0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgb25Db250YWluZXJNb3VzZU1vdmU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNYXhpbWl6ZWQgJiYgIXRoaXMuaXNUb29sYmFyU2hvd24pIHtcbiAgICAgICAgICAgIHRoaXMudG9nZ2xlVG9vbGJhcih0cnVlKTtcblxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMudG9vbGJhclRpbWVvdXQpO1xuXG4gICAgICAgICAgICB0aGlzLnRvb2xiYXJUaW1lb3V0ID0gc2V0VGltZW91dCh0aGlzLnRvZ2dsZVRvb2xiYXIuYmluZCh0aGlzKSwgdGhpcy5vcHRpb25zLnRpbWVvdXQpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIG9uQ29udGFpbmVySG92ZXI6IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICBpZiAodGhpcy5pc1BsYXlpbmcgJiYgIXRoaXMuaXNNYXhpbWl6ZWQpIHtcbiAgICAgICAgICAgIGlmIChldmVudC50eXBlID09PSAnbW91c2VvdmVyJykge1xuICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5wbGF5aW5nSW50ZXJ2YWwpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLnBsYXlpbmdJbnRlcnZhbCA9IHNldEludGVydmFsKHRoaXMubmV4dC5iaW5kKHRoaXMpLCB0aGlzLm9wdGlvbnMuaW50ZXJ2YWwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIG9uQ29udGFpbmVyQ2xpY2tPclRhcDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgaWYgKHRoaXMuaXNNYXhpbWl6ZWQpIHtcbiAgICAgICAgICAgIHRoaXMudG9nZ2xlVG9vbGJhcigpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy50b2dnbGVQbGF5KCk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgb25Db250YWluZXJEcmFnU3RhcnQ6IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICB2YXIgcHJldmlvdXM7XG5cbiAgICAgICAgdXRpbHMuYWRkQ2xhc3ModGhpcy5yZWZzLmNvbnRhaW5lciwgJ2RyYWcnKTtcblxuICAgICAgICAvLyBUb2RvOiBUaGlzIGNvZGUgcmVkdWNlcyB0aGUgcGVyZm9ybWFuY2VcbiAgICAgICAgcHJldmlvdXMgPSB0aGlzLnJlZnMuc2xpZGVzW3RoaXMuaW5kZXggLSAxXTtcblxuICAgICAgICBpZiAocHJldmlvdXMpIHtcbiAgICAgICAgICAgIHByZXZpb3VzLnN0eWxlW3V0aWxzLnRyYW5zZm9ybVByb3BlcnR5TmFtZV0gPSAndHJhbnNsYXRlM2QoLTEwMCUsMCwwKSc7XG4gICAgICAgICAgICBwcmV2aW91cy5zdHlsZS5vcGFjaXR5ID0gMTtcbiAgICAgICAgICAgIHByZXZpb3VzLnN0eWxlLnZpc2liaWxpdHkgPSAndmlzaWJsZSc7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgb25Db250YWluZXJEcmFnOiBmdW5jdGlvbiAoZXZlbnQsIGRlbHRhKSB7XG4gICAgICAgIHZhciBwcmV2aW91cywgY3VycmVudCwgbmV4dCwgcmF0aW87XG5cbiAgICAgICAgaWYgKE1hdGguYWJzKGRlbHRhKSA+IHRoaXMuY2xpZW50V2lkdGgpIHJldHVybjtcblxuICAgICAgICByYXRpbyA9IE1hdGguYWJzKGRlbHRhIC8gdGhpcy5jbGllbnRXaWR0aCk7XG5cbiAgICAgICAgcHJldmlvdXMgPSB0aGlzLnJlZnMuc2xpZGVzW3RoaXMuaW5kZXggLSAxXTtcbiAgICAgICAgY3VycmVudCA9IHRoaXMucmVmcy5zbGlkZXNbdGhpcy5pbmRleF07XG4gICAgICAgIG5leHQgPSB0aGlzLnJlZnMuc2xpZGVzW3RoaXMuaW5kZXggKyAxXTtcblxuICAgICAgICBpZiAoZGVsdGEgPCAwKSB7XG4gICAgICAgICAgICBjdXJyZW50LnN0eWxlW3V0aWxzLnRyYW5zZm9ybVByb3BlcnR5TmFtZV0gPSAndHJhbnNsYXRlM2QoJyArIGRlbHRhICsgJ3B4LDAsMCknO1xuXG4gICAgICAgICAgICBpZiAobmV4dCkge1xuICAgICAgICAgICAgICAgIG5leHQuc3R5bGUub3BhY2l0eSA9IHJhdGlvO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY3VycmVudC5zdHlsZS5vcGFjaXR5ID0gMSAtIHJhdGlvO1xuXG4gICAgICAgICAgICBpZiAocHJldmlvdXMpIHtcbiAgICAgICAgICAgICAgICBwcmV2aW91cy5zdHlsZVt1dGlscy50cmFuc2Zvcm1Qcm9wZXJ0eU5hbWVdID0gJ3RyYW5zbGF0ZTNkKCcgKyAoZGVsdGEgLSB0aGlzLmNsaWVudFdpZHRoKSArICdweCwwLDApJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBvbkNvbnRhaW5lckRyYWdFbmQ6IGZ1bmN0aW9uIChldmVudCwgZGVsdGEpIHtcbiAgICAgICAgdmFyIHByZXZpb3VzLCBjdXJyZW50LCBuZXh0LCByYXRpbywgdGhyZXNob2xkLCBhbGxvd2VkO1xuXG4gICAgICAgIGN1cnJlbnQgPSB0aGlzLnJlZnMuc2xpZGVzW3RoaXMuaW5kZXhdO1xuICAgICAgICBuZXh0ID0gdGhpcy5yZWZzLnNsaWRlc1t0aGlzLmluZGV4ICsgMV07XG4gICAgICAgIHByZXZpb3VzID0gdGhpcy5yZWZzLnNsaWRlc1t0aGlzLmluZGV4IC0gMV07XG5cbiAgICAgICAgcmF0aW8gPSBNYXRoLmFicyhkZWx0YSAvIHRoaXMuY2xpZW50V2lkdGgpO1xuICAgICAgICB0aHJlc2hvbGQgPSAwLjI1O1xuXG4gICAgICAgIHV0aWxzLnJlbW92ZUNsYXNzKHRoaXMucmVmcy5jb250YWluZXIsICdkcmFnJyk7XG5cbiAgICAgICAgYWxsb3dlZCA9IGRlbHRhIDwgMCAmJiBuZXh0IHx8IGRlbHRhID4gMCAmJiBwcmV2aW91cztcblxuICAgICAgICBpZiAocmF0aW8gPiB0aHJlc2hvbGQgJiYgYWxsb3dlZCkge1xuICAgICAgICAgICAgaWYgKGRlbHRhIDwgMCkge1xuICAgICAgICAgICAgICAgIGN1cnJlbnQuc3R5bGVbdXRpbHMudHJhbnNmb3JtUHJvcGVydHlOYW1lXSA9ICd0cmFuc2xhdGUzZCgtMTAwJSwwLDApJztcblxuICAgICAgICAgICAgICAgIG5leHQucmVtb3ZlQXR0cmlidXRlKCdzdHlsZScpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHByZXZpb3VzKSB7XG4gICAgICAgICAgICAgICAgICAgIHByZXZpb3VzLnJlbW92ZUF0dHJpYnV0ZSgnc3R5bGUnKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLm5leHQoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcHJldmlvdXMuc3R5bGVbdXRpbHMudHJhbnNmb3JtUHJvcGVydHlOYW1lXSA9ICd0cmFuc2xhdGUzZCgwLDAsMCknO1xuXG4gICAgICAgICAgICAgICAgY3VycmVudC5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7XG5cbiAgICAgICAgICAgICAgICBpZiAobmV4dCkge1xuICAgICAgICAgICAgICAgICAgICBuZXh0LnJlbW92ZUF0dHJpYnV0ZSgnc3R5bGUnKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLnByZXZpb3VzKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoZGVsdGEgPiAwICYmIHByZXZpb3VzKSB7XG4gICAgICAgICAgICAgICAgcHJldmlvdXMuc3R5bGVbdXRpbHMudHJhbnNmb3JtUHJvcGVydHlOYW1lXSA9ICd0cmFuc2xhdGUzZCgtMTAwJSwwLDApJztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKG5leHQpIHtcbiAgICAgICAgICAgICAgICBuZXh0LnJlbW92ZUF0dHJpYnV0ZSgnc3R5bGVzJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGN1cnJlbnQucmVtb3ZlQXR0cmlidXRlKCdzdHlsZScpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIG9uQ29udGFpbmVyVHJhbnNpdGlvbkVuZDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnRhcmdldC5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7XG4gICAgfSxcblxuICAgIG9uVG9vbGJhckhvdmVyOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNYXhpbWl6ZWQpIHtcbiAgICAgICAgICAgIGlmIChldmVudC50eXBlID09PSAnbW91c2VvdmVyJykge1xuICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRvb2xiYXJUaW1lb3V0KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy50b29sYmFyVGltZW91dCA9IHNldFRpbWVvdXQodGhpcy50b2dnbGVUb29sYmFyLmJpbmQodGhpcyksIDUwMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgb25Ub29sYmFyQ2xpY2tPclRhcDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciBuYW1lID0gZXZlbnQudGFyZ2V0Lm5hbWU7XG5cbiAgICAgICAgaWYgKG5hbWUgPT09IEJUTl9GSVJTVCkge1xuICAgICAgICAgICAgdGhpcy5maXJzdCgpO1xuICAgICAgICB9IGVsc2UgaWYgKG5hbWUgPT09IEJUTl9QUkVWSU9VUykge1xuICAgICAgICAgICAgdGhpcy5wcmV2aW91cygpO1xuICAgICAgICB9IGVsc2UgaWYgKG5hbWUgPT09IEJUTl9QTEFZQkFDSykge1xuICAgICAgICAgICAgdGhpcy50b2dnbGVQbGF5KCk7XG4gICAgICAgIH0gZWxzZSBpZiAobmFtZSA9PT0gQlROX05FWFQpIHtcbiAgICAgICAgICAgIHRoaXMubmV4dCgpO1xuICAgICAgICB9IGVsc2UgaWYgKG5hbWUgPT09IEJUTl9MQVNUKSB7XG4gICAgICAgICAgICB0aGlzLmxhc3QoKTtcbiAgICAgICAgfSBlbHNlIGlmIChuYW1lID09PSBCVE5fRlVMTFNDUkVFTikge1xuICAgICAgICAgICAgdGhpcy50b2dnbGVGdWxsc2NyZWVuKCk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgbGVuZ3RoOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlZnMuc2xpZGVzLmxlbmd0aDtcbiAgICB9LFxuXG4gICAgc2hvd0F0OiBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgICAgdmFyIGxlbmd0aDtcblxuICAgICAgICBsZW5ndGggPSB0aGlzLmxlbmd0aCgpO1xuICAgICAgICBpbmRleCA9IChpbmRleCA8IDApID8gbGVuZ3RoICsgaW5kZXggOiBpbmRleDtcblxuICAgICAgICBpZiAoaW5kZXggIT09IHRoaXMuaW5kZXggJiYgaW5kZXggPj0gMCAmJiBpbmRleCA8IGxlbmd0aCkge1xuICAgICAgICAgICAgdGhpcy5iZWZvcmVDaGFuZ2UoaW5kZXgsIHRoaXMuaW5kZXgsIGxlbmd0aCk7XG5cbiAgICAgICAgICAgIHV0aWxzLmFkZENsYXNzKHRoaXMucmVmcy5zbGlkZXNbaW5kZXhdLCAnc2hvdycpO1xuICAgICAgICAgICAgdXRpbHMucmVtb3ZlQ2xhc3ModGhpcy5yZWZzLnNsaWRlc1t0aGlzLmluZGV4XSwgJ3Nob3cnKTtcblxuICAgICAgICAgICAgdGhpcy5pbmRleCA9IGluZGV4O1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIG5leHQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zaG93QXQodGhpcy5pbmRleCArIDEpO1xuICAgIH0sXG5cbiAgICBwcmV2aW91czogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5pbmRleCkge1xuICAgICAgICAgICAgdGhpcy5zaG93QXQodGhpcy5pbmRleCAtIDEpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGZpcnN0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2hvd0F0KDApO1xuICAgIH0sXG5cbiAgICBsYXN0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2hvd0F0KC0xKTtcbiAgICB9LFxuXG4gICAgYmVmb3JlQ2hhbmdlOiBmdW5jdGlvbiAobmV3SW5kZXgsIGluZGV4LCBsZW5ndGgpIHtcbiAgICAgICAgdmFyIGRpc2FibGVkLCBsYXN0O1xuXG4gICAgICAgIGxhc3QgPSBsZW5ndGggLSAxO1xuXG4gICAgICAgIGlmICgoZGlzYWJsZWQgPSBuZXdJbmRleCA9PT0gMCkgfHwgaW5kZXggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMucmVmcy5idXR0b25zW0JUTl9GSVJTVF0uZGlzYWJsZWQgPSBkaXNhYmxlZDtcbiAgICAgICAgICAgIHRoaXMucmVmcy5idXR0b25zW0JUTl9QUkVWSU9VU10uZGlzYWJsZWQgPSBkaXNhYmxlZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICgoZGlzYWJsZWQgPSBuZXdJbmRleCA9PT0gbGFzdCkgfHwgaW5kZXggPT09IGxhc3QpIHtcbiAgICAgICAgICAgIHRoaXMucmVmcy5idXR0b25zW0JUTl9ORVhUXS5kaXNhYmxlZCA9IGRpc2FibGVkO1xuICAgICAgICAgICAgdGhpcy5yZWZzLmJ1dHRvbnNbQlROX0xBU1RdLmRpc2FibGVkID0gZGlzYWJsZWQ7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmlzUGxheWluZyAmJiBuZXdJbmRleCA9PT0gbGFzdCkge1xuICAgICAgICAgICAgdGhpcy50b2dnbGVQbGF5KGZhbHNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMucmVmcy5zbGlkZXNbaW5kZXhdLnN0eWxlLnZpc2liaWxpdHkgPSAndmlzaWJsZSc7IC8vIGZvciBwcm9wZXIgYW5pbWF0aW9uXG4gICAgfSxcblxuICAgIHRvZ2dsZVRvb2xiYXI6IGZ1bmN0aW9uIChzaG93KSB7XG4gICAgICAgIHNob3cgPSB1dGlscy50b2dnbGUoc2hvdywgdGhpcy5pc1Rvb2xiYXJTaG93bik7XG5cbiAgICAgICAgaWYgKHRoaXMuaXNUb29sYmFyU2hvd24gIT09IHNob3cpIHtcbiAgICAgICAgICAgIHRoaXMuaXNUb29sYmFyU2hvd24gPSBzaG93O1xuXG4gICAgICAgICAgICBpZiAoc2hvdykge1xuICAgICAgICAgICAgICAgIHV0aWxzLmFkZENsYXNzKHRoaXMucmVmcy50b29sYmFyLCAnc2hvdycpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB1dGlscy5yZW1vdmVDbGFzcyh0aGlzLnJlZnMudG9vbGJhciwgJ3Nob3cnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICB0b2dnbGVGdWxsc2NyZWVuOiBmdW5jdGlvbiAobWF4aW1pemUpIHtcbiAgICAgICAgbWF4aW1pemUgPSB1dGlscy50b2dnbGUobWF4aW1pemUsIHRoaXMuaXNNYXhpbWl6ZWQpO1xuXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZnVsbHNjcmVlbiAmJiB0aGlzLmlzTWF4aW1pemVkICE9PSBtYXhpbWl6ZSkge1xuICAgICAgICAgICAgdGhpcy5pc01heGltaXplZCA9IG1heGltaXplO1xuXG4gICAgICAgICAgICBpZiAobWF4aW1pemUpIHtcbiAgICAgICAgICAgICAgICB1dGlscy5hZGRDbGFzcyh0aGlzLmVsLCAnbWF4aW1pemUnKTtcbiAgICAgICAgICAgICAgICB1dGlscy5hZGRDbGFzcyhkb2N1bWVudC5ib2R5LCAnbm9zY3JvbGwnKTtcblxuICAgICAgICAgICAgICAgIC8vIHRoaXMudG9nZ2xlVG9vbGJhcih0cnVlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdXRpbHMucmVtb3ZlQ2xhc3ModGhpcy5lbCwgJ21heGltaXplJyk7XG4gICAgICAgICAgICAgICAgdXRpbHMucmVtb3ZlQ2xhc3MoZG9jdW1lbnQuYm9keSwgJ25vc2Nyb2xsJyk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZVRvb2xiYXIoZmFsc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHRvZ2dsZVBsYXk6IGZ1bmN0aW9uIChwbGF5KSB7XG4gICAgICAgIHBsYXkgPSB1dGlscy50b2dnbGUocGxheSwgdGhpcy5pc1BsYXlpbmcpO1xuXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuc2xpZGVzaG93ICYmIHRoaXMuaXNQbGF5aW5nICE9PSBwbGF5KSB7XG4gICAgICAgICAgICB0aGlzLmlzUGxheWluZyA9IHBsYXk7XG5cbiAgICAgICAgICAgIGlmIChwbGF5KSB7XG4gICAgICAgICAgICAgICAgdXRpbHMuYWRkQ2xhc3ModGhpcy5lbCwgJ3BsYXknKTtcblxuICAgICAgICAgICAgICAgIHRoaXMucGxheWluZ0ludGVydmFsID0gc2V0SW50ZXJ2YWwodGhpcy5uZXh0LmJpbmQodGhpcyksIHRoaXMub3B0aW9ucy5pbnRlcnZhbCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHV0aWxzLnJlbW92ZUNsYXNzKHRoaXMuZWwsICdwbGF5Jyk7XG5cbiAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMucGxheWluZ0ludGVydmFsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0pO1xuIiwidmFyIHV0aWxzID0gcmVxdWlyZSgnLi91dGlscycpO1xuXG5mdW5jdGlvbiBXaWRnZXQob3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSB0aGlzLmdldE9wdGlvbnMob3B0aW9ucyk7XG5cbiAgICB0aGlzLmVsID0gb3B0aW9ucy5lbCB8fCB0aGlzLmNyZWF0ZUVsZW1lbnQoKTtcblxuICAgIHRoaXMuZGF0YSA9IG9wdGlvbnMuZGF0YTtcbiAgICB0aGlzLm9wdGlvbnMgPSBvcHRpb25zO1xuXG4gICAgaWYgKHV0aWxzLmlzRnVuY3Rpb24odGhpcy5pbml0aWFsaXplKSkge1xuICAgICAgICB0aGlzLmluaXRpYWxpemUob3B0aW9ucyk7XG4gICAgfVxufVxuXG5XaWRnZXQucHJvdG90eXBlID0ge1xuXG4gICAgZ2V0T3B0aW9uczogZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgdmFyIGZyb21BdHRyaWJ1dGVzID0ge307XG5cbiAgICAgICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgICAgICAgaWYgKCdlbCcgaW4gb3B0aW9ucykge1xuICAgICAgICAgICAgaWYgKCF1dGlscy5pc0VsZW1lbnQob3B0aW9ucy5lbCkpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJzxlbD4gbXVzdCBiZSBhIERPTSBlbGVtZW50Jyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZyb21BdHRyaWJ1dGVzID0gdGhpcy5wYXJzZUF0dHJpYnV0ZXMob3B0aW9ucy5lbCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdXRpbHMubWVyZ2Uoe30sIHRoaXMuZGVmYXVsdHMsIGZyb21BdHRyaWJ1dGVzLCBvcHRpb25zKTtcbiAgICB9LFxuXG4gICAgY3JlYXRlRWxlbWVudDogZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgdmFyIHRhZ05hbWUsIGVsO1xuXG4gICAgICAgIHRhZ05hbWUgPSB0aGlzLnRhZ05hbWUgfHwgJ2Rpdic7XG5cbiAgICAgICAgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KHRhZ05hbWUpO1xuXG4gICAgICAgIGlmICh0aGlzLmNsYXNzTmFtZSkge1xuICAgICAgICAgICAgZWwuY2xhc3NOYW1lID0gdGhpcy5jbGFzc05hbWU7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZWw7XG4gICAgfSxcblxuICAgIHBhcnNlQXR0cmlidXRlczogZnVuY3Rpb24gKGVsKSB7XG4gICAgICAgIHZhciBhdHRyaWJ1dGVzLCBhdHRyaWJ1dGUsIGtleSwgdmFsdWUsIGksIGRhdGE7XG5cbiAgICAgICAgZGF0YSA9IHt9O1xuXG4gICAgICAgIGF0dHJpYnV0ZXMgPSBlbC5hdHRyaWJ1dGVzO1xuXG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBhdHRyaWJ1dGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBhdHRyaWJ1dGUgPSBhdHRyaWJ1dGVzW2ldO1xuXG4gICAgICAgICAgICBtYXRjaGVzID0gYXR0cmlidXRlLm5hbWUubWF0Y2goL15kYXRhLSguKykkLyk7XG5cbiAgICAgICAgICAgIGlmIChtYXRjaGVzKSB7XG4gICAgICAgICAgICAgICAga2V5ID0gbWF0Y2hlc1sxXTtcblxuICAgICAgICAgICAgICAgIGlmIChrZXkgaW4gdGhpcy5kZWZhdWx0cykge1xuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBKU09OLnBhcnNlKGF0dHJpYnV0ZS52YWx1ZS50b0xvd2VyQ2FzZSgpKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlICE9IG51bGwpIGRhdGFba2V5XSA9IHZhbHVlO1xuICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdDYW4gbm90IHBhcnNlIFsnICsgYXR0cmlidXRlLm5hbWUgKyAnXSBhdHRyaWJ1dGUnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgIH0sXG5cbiAgICBiaW5kUmVmczogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodXRpbHMuaXNGdW5jdGlvbih0aGlzLnJlZnMpKSB7XG4gICAgICAgICAgICB0aGlzLnJlZnMgPSB0aGlzLnJlZnMoKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBhcHBlbmRIVE1MOiBmdW5jdGlvbiAoaHRtbCkge1xuICAgICAgICB0aGlzLmVsLmlubmVySFRNTCA9IGh0bWw7XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgaHRtbDtcblxuICAgICAgICBpZiAodXRpbHMuaXNGdW5jdGlvbih0aGlzLnRlbXBsYXRlKSkge1xuICAgICAgICAgICAgaHRtbCA9IHRoaXMudGVtcGxhdGUoKTtcblxuICAgICAgICAgICAgdGhpcy5hcHBlbmRIVE1MKGh0bWwpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5iaW5kUmVmcygpO1xuICAgICAgICB0aGlzLmJpbmRFdmVudHMoKTtcblxuICAgICAgICBpZiAodXRpbHMuaXNGdW5jdGlvbih0aGlzLmFmdGVyUmVuZGVyKSkge1xuICAgICAgICAgICAgdGhpcy5hZnRlclJlbmRlcigpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfSxcblxuICAgIGRlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHV0aWxzLmlzRnVuY3Rpb24odGhpcy51bmJpbmRFdmVudHMpKSB7XG4gICAgICAgICAgICB0aGlzLnVuYmluZEV2ZW50cygpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuZWwucGFyZW50Tm9kZSkge1xuICAgICAgICAgICAgdGhpcy5lbC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHRoaXMuZWwpO1xuICAgICAgICB9XG5cbiAgICAgICAgZGVsZXRlIHRoaXMuZWw7XG4gICAgICAgIGRlbGV0ZSB0aGlzLnJlZnM7XG4gICAgICAgIGRlbGV0ZSB0aGlzLm9wdGlvbnM7XG4gICAgICAgIGRlbGV0ZSB0aGlzLmRhdGE7XG4gICAgfVxuXG59O1xuXG5XaWRnZXQuZXh0ZW5kID0gZnVuY3Rpb24gKHByb3BlcnRpZXMpIHtcbiAgICB2YXIgUGFyZW50LCBDaGlsZDtcblxuICAgIFBhcmVudCA9IHRoaXM7XG5cbiAgICBDaGlsZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgUGFyZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgfTtcblxuICAgIHV0aWxzLm1lcmdlKENoaWxkLnByb3RvdHlwZSwgUGFyZW50LnByb3RvdHlwZSwgcHJvcGVydGllcyk7XG5cbiAgICByZXR1cm4gQ2hpbGQ7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFdpZGdldDtcbiIsIndpbmRvdy5QcmVzZW50YXRpb24gPSByZXF1aXJlKCcuL1ByZXNlbnRhdGlvbicpO1xuIiwidmFyIHV0aWxzID0ge30sIHByZWZpeGVzID0gWydXZWJraXQnLCAnTW96JywgJ08nLCAnbXMnXTtcblxudXRpbHMudGVzdFN0eWxlUHJvcGVydHkgPSBmdW5jdGlvbiAocHJvcGVydHkpIHtcbiAgICB2YXIgdWNQcm9wZXJ0eSwgcHJvcGVydGllcywgaTtcblxuICAgIHVjUHJvcGVydHkgPSBwcm9wZXJ0eS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHByb3BlcnR5LnNsaWNlKDEpO1xuXG4gICAgcHJvcGVydGllcyA9IChwcm9wZXJ0eSArICcgJyArIHByZWZpeGVzLmpvaW4odWNQcm9wZXJ0eSArICcgJykgKyB1Y1Byb3BlcnR5KS5zcGxpdCgnICcpO1xuXG4gICAgZm9yIChpID0gMDsgaSA8IHByb3BlcnRpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKHByb3BlcnRpZXNbaV0gaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlKSB7XG4gICAgICAgICAgICByZXR1cm4gcHJvcGVydGllc1tpXTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5cbnV0aWxzLnN1cHBvcnRNb2JpbGUgPSAoZnVuY3Rpb24gKCkge1xuICAgIHZhciByZWdleHAsIHVzZXJBZ2VudDtcblxuICAgIHJlZ2V4cCA9IC9hbmRyb2lkfHdlYm9zfGlwaG9uZXxpcGFkfGlwb2R8YmxhY2tiZXJyeXxpZW1vYmlsZXxvcGVyYSBtaW5pL2k7XG4gICAgdXNlckFnZW50ID0gbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpO1xuXG4gICAgcmV0dXJuIHJlZ2V4cC50ZXN0KHVzZXJBZ2VudCk7XG59KSgpO1xuXG51dGlscy5zdXBwb3J0TVNQb2ludGVyID0gKGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gd2luZG93Lm5hdmlnYXRvci5tc1BvaW50ZXJFbmFibGVkID09PSB0cnVlO1xufSkoKTtcblxudXRpbHMuc3VwcG9ydFRvdWNoID0gKGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gJ29udG91Y2hzdGFydCcgaW4gd2luZG93IHx8IHdpbmRvdy5Eb2N1bWVudFRvdWNoICYmIGRvY3VtZW50IGluc3RhbmNlb2YgRG9jdW1lbnRUb3VjaFxufSkoKTtcblxudXRpbHMuc3VwcG9ydENsYXNzTGlzdCA9IChmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuICdjbGFzc0xpc3QnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcbn0pKCk7XG5cbnV0aWxzLnRyYW5zZm9ybVByb3BlcnR5TmFtZSA9IChmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHV0aWxzLnRlc3RTdHlsZVByb3BlcnR5KCd0cmFuc2Zvcm0nKTtcbn0pKHV0aWxzKTtcblxudXRpbHMudHJhbnNpdGlvblByb3BlcnR5TmFtZSA9IChmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHV0aWxzLnRlc3RTdHlsZVByb3BlcnR5KCd0cmFuc2l0aW9uJyk7XG59KSh1dGlscyk7XG5cbnV0aWxzLnRyYW5zaXRpb25FbmRFdmVudE5hbWUgPSAoZnVuY3Rpb24gKCkge1xuICAgIHZhciBtYXAsIGtleTtcblxuICAgIG1hcCA9IHtcbiAgICAgICAgJ3RyYW5zaXRpb24nOiAndHJhbnNpdGlvbmVuZCcsXG4gICAgICAgICdPVHJhbnNpdGlvbic6ICdvVHJhbnNpdGlvbkVuZCcsXG4gICAgICAgICdtb3pUcmFuc2l0aW9uJzogJ3RyYW5zaXRpb25lbmQnLFxuICAgICAgICAnV2Via2l0VHJhbnNpdGlvbic6ICd3ZWJraXRUcmFuc2l0aW9uRW5kJ1xuICAgIH07XG5cbiAgICBmb3IgKGtleSBpbiBtYXApIHtcbiAgICAgICAgaWYgKGtleSBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUpIHtcbiAgICAgICAgICAgIHJldHVybiBtYXBba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn0pKCk7XG5cbnV0aWxzLm1lcmdlID0gZnVuY3Rpb24gKG9iamVjdCkge1xuICAgIHZhciBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcblxuICAgIGFyZ3MuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gaXRlbSkgb2JqZWN0W2tleV0gPSBpdGVtW2tleV07XG4gICAgfSk7XG5cbiAgICByZXR1cm4gb2JqZWN0O1xufTtcblxudXRpbHMudG9nZ2xlID0gZnVuY3Rpb24gKHZhbHVlLCBwcm9wZXJ0eSkge1xuICAgIGlmIChwcm9wZXJ0eSAhPSBudWxsKSB7XG4gICAgICAgIHZhbHVlID0gIXByb3BlcnR5O1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHZhbHVlID0gdmFsdWUgPT09IHRydWU7XG4gICAgfVxuXG4gICAgcmV0dXJuIHZhbHVlO1xufTtcblxudXRpbHMuZGVib3VuY2UgPSBmdW5jdGlvbiAoY2FsbGJhY2ssIHdhaXQpIHtcbiAgICB2YXIgdGltZW91dDtcblxuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBjb250ZXh0LCBhcmdzO1xuXG4gICAgICAgIGNvbnRleHQgPSB0aGlzO1xuICAgICAgICBhcmdzID0gYXJndW1lbnRzO1xuXG4gICAgICAgIGNsZWFyVGltZW91dCh0aW1lb3V0KTtcblxuICAgICAgICB0aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjYWxsYmFjay5hcHBseShjb250ZXh0LCBhcmdzKTtcbiAgICAgICAgfSwgd2FpdCk7XG4gICAgfVxufTtcblxudXRpbHMuaXNFbGVtZW50ID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUubm9kZVR5cGUgPT09IDE7XG59O1xuXG51dGlscy5pc0Z1bmN0aW9uID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlICYmIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh2YWx1ZSkgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG59O1xuXG51dGlscy5hZGRDbGFzcyA9IChmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHV0aWxzLnN1cHBvcnRDbGFzc0xpc3QpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChlbCwgbmFtZSkge1xuICAgICAgICAgICAgZWwuY2xhc3NMaXN0LmFkZChuYW1lKTtcbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoZWwsIG5hbWUpIHtcbiAgICAgICAgICAgIHZhciBjbGFzc0xpc3Q7XG5cbiAgICAgICAgICAgIG5hbWUgPSBuYW1lLnRyaW0oKTtcbiAgICAgICAgICAgIGNsYXNzTGlzdCA9IGVsLmNsYXNzTmFtZS50cmltKCkuc3BsaXQoL1xccysvKTtcblxuICAgICAgICAgICAgaWYgKGNsYXNzTGlzdC5pbmRleE9mKG5hbWUpID09PSAtMSkge1xuICAgICAgICAgICAgICAgIGNsYXNzTGlzdC5wdXNoKG5hbWUpO1xuXG4gICAgICAgICAgICAgICAgZWwuY2xhc3NOYW1lID0gY2xhc3NMaXN0LmpvaW4oJyAnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0pKHV0aWxzKTtcblxudXRpbHMucmVtb3ZlQ2xhc3MgPSAoZnVuY3Rpb24gKCkge1xuICAgIGlmICh1dGlscy5zdXBwb3J0Q2xhc3NMaXN0KSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoZWwsIG5hbWUpIHtcbiAgICAgICAgICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUobmFtZSk7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGVsLCBuYW1lKSB7XG4gICAgICAgICAgICB2YXIgY2xhc3NMaXN0LCBpbmRleDtcblxuICAgICAgICAgICAgbmFtZSA9IG5hbWUudHJpbSgpO1xuICAgICAgICAgICAgY2xhc3NMaXN0ID0gZWwuY2xhc3NOYW1lLnRyaW0oKS5zcGxpdCgvXFxzKy8pO1xuXG4gICAgICAgICAgICBpbmRleCA9IGNsYXNzTGlzdC5pbmRleE9mKG5hbWUpO1xuXG4gICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgY2xhc3NMaXN0LnNwbGljZShpbmRleCwgMSk7XG5cbiAgICAgICAgICAgICAgICBlbC5jbGFzc05hbWUgPSBjbGFzc0xpc3Quam9pbignICcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSkodXRpbHMpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHV0aWxzO1xuIl19
